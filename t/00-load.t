#!perl -T

use Test::More tests => 1;

BEGIN {
	use_ok( 'Net::Indosoft::Voicebridge' );
}

diag( "Testing Net::Indosoft::Voicebridge $Net::Indosoft::Voicebridge::VERSION, Perl $], $^X" );
