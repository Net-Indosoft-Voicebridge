#!perl

use Test::More;

open URL, "t/URL"
  or do { plan 'skip_all' => 'Put your URL in the t/URL file to test'; exit };
chomp( my $URL = scalar(<URL>) );

plan tests => 6 + 2 + 2 + 2 + 2 + 2 + 2 + 2 + 2;

use Net::Indosoft::Voicebridge;

my $vb = new Net::Indosoft::Voicebridge( 'url' => $URL );

my $return;

# addAccount (6)

$return = $vb->addAccount(
  'account_name'     => 'aname', #i think this is more like a username
  'account_password' => 'test54',

  'account_desc'     => 'A test account',

  'account_addr'     => '1234 Test Lane',
  'account_city'     => 'Testville',
  'account_state'    => 'CA',
  'account_zip'      => '54321',
  'account_country'  => 'US',

  'account_phone'    => '415-555-1212',
#  'account_fax'      => '415-555-1212',
  'account_email'    => 'ivan-voicebridge-test@freeside.biz',
);

ok( ! $return->{error}, 'addAccount successful' ) or warn $return->{error};

like($return->{account_id}, qr/^(\d+)$/, 'addAccount returned account_id');

my $account_id = $return->{account_id};

$return = $vb->addAccount(
);

ok( $return->{error}, 'addAccount empty error test', );

like($return->{error}, qr/Please, fill in/, 'addAccount empty error test got expected error');

$return = $vb->addAccount(
  #'account_name'     => 'A. Name',
  'account_desc'     => 'A test account',
  'account_addr'     => '1234 Test Lane',
  'account_city'     => 'Testville',
  'account_state'    => 'CA',
  'account_zip'      => '54321',
  #'account_phone'    => '415-555-1212',
  'account_fax'      => '415-555-1212',
  #'account_email'    => 'ivan-voicebridge-test@freeside.biz',
  #'account_password' => 'testing',
);

ok( $return->{error}, 'addAccount bad data error test', );

like($return->{error}, qr/Please, fill in/, 'addAccount bad data error test got expected error');

# modifyAccount (2)

$return = $vb->modifyAccount(
  'account_id'       => $account_id,
  'account_name'     => 'anewname', #i think this is more like a username
  'account_password' => 'test32',

  'account_desc'     => 'A modified test account',

  'account_addr'     => '5432 Test Lane',
  'account_city'     => 'Test City',
  'account_state'    => 'NY',
  'account_zip'      => '12345',
  'account_country'  => 'US',

  'account_phone'    => '212-555-1212',
  'account_fax'      => '212-555-1212',
  'account_email'    => 'ivan-voicebridge-test2@freeside.biz',
);

ok( ! $return->{error}, 'modifyAccount successful' ) or warn $return->{error};

is($return->{account_id}, $account_id, 'modifyAccount returned account_id');

# addClient (2)

$return = $vb->addClient(
  'account_id'              => $account_id,

  'client_contact_name'     => 'Tofu Beast',
  'client_contact_password' => 'word54',

  'client_contact_addr'     => '5432 Test Lane Suite 11',
  'client_contact_city'     => 'Test City',
  'client_contact_state'    => 'NY',
  'client_contact_country'  => 'US',
  'client_contact_zip'      => '12345',

  'client_contact_phone'    => '212-555-1234',
  'client_contact_fax'      => '212-555-1234',
  'client_contact_email'    => 'ivan-voicebridge-test3@freeside.biz',

);

ok( ! $return->{error}, 'addClient successful' ) or warn $return->{error};

like($return->{client_id}, qr/^(\d+)$/, 'addClient returned client_id');

my $client_id = $return->{client_id};

# modifyClient (2)

$return = $vb->modifyClient(
  'account_id'              => $account_id,
  'client_id'               => $client_id,

  'client_contact_name'     => 'Secret Agent X',
  'client_contact_password' => 'word32',

  'client_contact_addr'     => '1234 Test Lane Suite 55',
  'client_contact_city'     => 'Testville',
  'client_contact_state'    => 'CA',
  'client_contact_country'  => 'US',
  'client_contact_zip'      => '54321',

  'client_contact_phone'    => '415-555-1234',
  'client_contact_fax'      => '415-555-1234',
  'client_contact_email'    => 'ivan-voicebridge-test4@freeside.biz',
  
);

ok( ! $return->{error}, 'modifyClient successful' ) or warn $return->{error};

is($return->{client_id}, $client_id, 'modifyClient returned client_id');

# addConference (2)

$return = $vb->addConference(
  'client_id'          => $client_id,
  'conference_name'    => 'Test Conference',
  'conference_desc'    => 'Net Indosoft Voicebridge test',
  'start_time'         => '2010-20-04 16:20:00',
  #'moderated_flag'     => 0,
  #'entry_ann_flag'     => 0
  #'record_flag'        => 0
  #'moh_flag'           => 0
  #'talk_detect_flag'   => 0
  #'play_user_cnt_flag' => 0
  #'wait_for_admin'     => 0
  #'stop_on_admin_exit' => 0
  #'second_pin'         => 0
  #'secondary_pin'      => 0,
  #'allow_sub-conf'     => 0,
  #'duration'           => 0,
  #'conference_type' => 'reservation', #'reservationless',

);

ok( ! $return->{error}, 'addConference successful' ) or warn $return->{error};

like($return->{conference_id}, qr/^(\d+)$/, 'addConference returned conference_id');

my $conference_id = $return->{conference_id};

# modifyConference (2)

$return = $vb->modifyConference(
  'conference_id'      => $conference_id,
  'conference_name'    => 'Modified Test Conference',
  'conference_desc'    => 'Net Indosoft Voicebridge modify test',
  'start_time'         => '2010-20-04 16:20:00',
  'moderated_flag'     => 1,
  #'entry_ann_flag'     => 0
  #'record_flag'        => 0
  #'moh_flag'           => 0
  #'talk_detect_flag'   => 0
  #'play_user_cnt_flag' => 0
  'wait_for_admin'     => 1
  #'stop_on_admin_exit' => 0
  #'second_pin'         => 0
  #'secondary_pin'      => 0,
  #'allow_sub-conf'     => 0,
  #'duration'           => 0,
  #'conference_type' => 'reservation', #'reservationless',

);

ok( ! $return->{error}, 'modifyConference successful' ) or warn $return->{error};

is($return->{conference_id}, $conference_id, 'modifyConference returned conference_id')
  or warn $return->{'return'};

# deleteConference (2)

$return = $vb->deleteConference(
  'conference_id' => $conference_id,
);

ok( ! $return->{error}, 'deleteConference successful' )
  or warn $return->{error}. "\n". $return->{return}. "\n";

TODO: {

  local $TODO = 'deleteConference always returns "Deleted successfully", even when nothing was';

  $return = $vb->deleteConference(
    'conference_id' => '999999999',
  );

  ok( $return->{error}, 'deleteConference bad data error test' )
    ;#or warn $return->{return};

}

# deleteClient (2)

$return = $vb->deleteClient(
  'client_id' => $client_id,
);

ok( ! $return->{error}, 'deleteClient successful' )
  or warn $return->{error}. "\n". $return->{return}. "\n";

TODO: {

  local $TODO = 'deleteClient always returns "Deleted successfully", even when nothing was';

  $return = $vb->deleteClient(
    'client_id' => '999999999',
  );

  ok( $return->{error}, 'deleteClient bad data error test' )
    ;#or warn $return->{return};

}

# deleteAccount (2)

$return = $vb->deleteAccount(
  'account_id' => $account_id,
);

ok( ! $return->{error}, 'deleteAccount successful' )
  or warn $return->{error}. "\n". $return->{return}. "\n";

TODO: {

  local $TODO = 'deleteAccount always returns "Deleted successfully", even when nothing was';

  $return = $vb->deleteAccount(
    'account_id' => '999999999',
  );

  ok( $return->{error}, 'deleteAccount bad data error test' )
    ;#or warn $return->{return};

}
